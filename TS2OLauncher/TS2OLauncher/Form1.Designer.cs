﻿namespace PDPatcher
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PrgTotal = new System.Windows.Forms.ProgressBar();
            this.PrgFile = new System.Windows.Forms.ProgressBar();
            this.LblSpeed = new System.Windows.Forms.Label();
            this.LblDownloading = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // PrgTotal
            // 
            this.PrgTotal.Location = new System.Drawing.Point(19, 433);
            this.PrgTotal.Name = "PrgTotal";
            this.PrgTotal.Size = new System.Drawing.Size(631, 23);
            this.PrgTotal.TabIndex = 0;
            // 
            // PrgFile
            // 
            this.PrgFile.Location = new System.Drawing.Point(19, 404);
            this.PrgFile.Name = "PrgFile";
            this.PrgFile.Size = new System.Drawing.Size(631, 23);
            this.PrgFile.TabIndex = 0;
            // 
            // LblSpeed
            // 
            this.LblSpeed.AutoSize = true;
            this.LblSpeed.Location = new System.Drawing.Point(16, 388);
            this.LblSpeed.Name = "LblSpeed";
            this.LblSpeed.Size = new System.Drawing.Size(47, 13);
            this.LblSpeed.TabIndex = 2;
            this.LblSpeed.Text = "Kb/Sec:";
            // 
            // LblDownloading
            // 
            this.LblDownloading.AutoSize = true;
            this.LblDownloading.Location = new System.Drawing.Point(16, 364);
            this.LblDownloading.Name = "LblDownloading";
            this.LblDownloading.Size = new System.Drawing.Size(72, 13);
            this.LblDownloading.TabIndex = 4;
            this.LblDownloading.Text = "Downloading:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 21.75F);
            this.label1.Location = new System.Drawing.Point(60, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(238, 40);
            this.label1.TabIndex = 5;
            this.label1.Text = "The Sims 2 Online";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::PDPatcher.Properties.Resources.tso_avatar;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(19, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 40);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(24)))), ((int)(((byte)(24)))));
            this.ClientSize = new System.Drawing.Size(670, 468);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LblDownloading);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.LblSpeed);
            this.Controls.Add(this.PrgFile);
            this.Controls.Add(this.PrgTotal);
            this.ForeColor = System.Drawing.Color.White;
            this.Name = "Form1";
            this.Text = "PD Patcher";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar PrgTotal;
        private System.Windows.Forms.ProgressBar PrgFile;
        private System.Windows.Forms.Label LblSpeed;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label LblDownloading;
        private System.Windows.Forms.Label label1;
    }
}

